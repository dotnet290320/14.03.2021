using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyPostgres
{
    class Program
    {
        static bool TestDbConnection(string conn)
        {
            try 
            {
                using(var my_conn = new NpgsqlConnection(conn))
                {
                    my_conn.Open();
                    return true;
                }
            }
            catch (Exception ex)
            {
                // write error to log file
                return false;
            }
        }

       
        private static int Run_sp_GetRandomNumber(string conn_string, int limit)
        {
            try
            {
                // request a connection

                using (var conn = new NpgsqlConnection(conn_string))
                {
                    conn.Open();
                    string sp_name = "a_sp_get_randoms";
                    // "call procedure_name(a,b,c...)"

                    NpgsqlCommand command = new NpgsqlCommand(sp_name, conn);
                    command.CommandType = System.Data.CommandType.StoredProcedure; // this is default

                    command.Parameters.AddRange(new NpgsqlParameter[]
                    {
                    new NpgsqlParameter("_max", limit)
                    });

                    var reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        int random_number = (int)reader["a_sp_get_randoms"];
                        // ....
                        return random_number;
                    }
                    throw new ApplicationException("Function not returned value!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine($"Function a_sp_get_randoms failed. parameters: {limit}");
                return 0;
            }
            finally
            {
                // return connection
            }
        }

        private static void GetAllMovies()
        {
            NpgsqlConnection cs = null;
            try
            {
                cs = MyConnectionPool.Instance.GetConnection();
                string query = "SELECT * FROM movies";

                NpgsqlCommand command = new NpgsqlCommand(query, cs);
                command.CommandType = System.Data.CommandType.Text; // this is default

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    long id = (long)reader["id"];
                    string title = (string)reader["title"];
                    // ....
                    //Console.WriteLine($"{id} {title}");
                }
            }
            finally
            {
                // return connection
                cs.Dispose(); // or close?
                MyConnectionPool.Instance.ReturnConnection(cs);
            }
        }


        private static int Run_sp_GetRandomNumber_wht_connection_pool(int limit)
        {
            NpgsqlConnection cs = null;
            try
            {
                // request a connection

                //using (var conn = new NpgsqlConnection(conn_string))
                //{
                //cs.Open();

                 cs = MyConnectionPool.Instance.GetConnection();

                    string sp_name = "a_sp_get_randoms";
                    // "call procedure_name(a,b,c...)"

                    NpgsqlCommand command = new NpgsqlCommand(sp_name, cs);
                    command.CommandType = System.Data.CommandType.StoredProcedure; // this is default

                    command.Parameters.AddRange(new NpgsqlParameter[]
                    {
                    new NpgsqlParameter("_max", limit)
                    });

                    var reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        int random_number = (int)reader["a_sp_get_randoms"];
                        // ....
                        return random_number;
                    }
                    throw new ApplicationException("Function not returned value!");
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine($"Function a_sp_get_randoms failed. parameters: {limit}");
                return 0;
            }
            finally
            {
                // return connection
                cs.Dispose(); // or close?
                MyConnectionPool.Instance.ReturnConnection(cs);
            }
        }

        static void Main(string[] args)
        {
            // read from config file
            string conn_string = "Host=localhost;Username=postgres;Password=admin;Database=postgres";

            ManualResetEvent mre = new ManualResetEvent(false);
            // on systme startup
            if (TestDbConnection(conn_string))
            {

                Stopwatch sw = new Stopwatch();
                sw.Start();
                List<Task> tasks = new List<Task>();
                for (int i = 0; i < 10000; i++)
                {
                    int j = i;
                    tasks.Add( Task.Run(
                        () =>
                        {
                            //Thread.Sleep(5);
                            mre.WaitOne(); // just for fun
                            GetAllMovies
                        }));
                }
                Console.ReadLine();
                mre.Set();
                Task.WaitAll(tasks.ToArray());
                sw.Stop();
                Console.WriteLine(sw.ElapsedTicks);
              
            }
            else
            {
                Console.WriteLine("Cannot connect to db!");
            }
        }

        
    }
}
