﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyPostgres
{
    public class MyConnectionPool
    {

        private Queue<NpgsqlConnection> m_connections;
        // TODO: change this! should come from config file!
        private static string conn_string = "Host=localhost;Username=postgres;Password=admin;Database=postgres"; 
        private static MyConnectionPool INSTANCE;

        private static object key = new object();

        public static MyConnectionPool Instance
        {
            get
            {
                if (INSTANCE == null)
                {
                    lock (key)
                    {
                        if (INSTANCE == null)
                        {
                            // TODO: change this! should come from config file - the number of possible connections!
                            INSTANCE = new MyConnectionPool(40);
                        }
                    }
                }
                return INSTANCE;
            }
        }

        private MyConnectionPool(int capacity)
        {
            m_connections = new Queue<NpgsqlConnection>(capacity);
            

            for (int i = 0; i < capacity; i++)
            {
                m_connections.Enqueue(new NpgsqlConnection(conn_string));
            }
        }

        public NpgsqlConnection GetConnection()
        {
            lock(key)
            {
                while (m_connections.Count == 0)
                {
                    Monitor.Wait(key);
                }
                NpgsqlConnection result = m_connections.Dequeue();
                //result.Open(); // check if not failed, if so create new connection
                result = new NpgsqlConnection(conn_string); // is this better?
                result.Open();
                return result;
            }
        }
        public void ReturnConnection(NpgsqlConnection conn)
        {
            lock (key)
            {
                //conn.Close(); // here? or maybe somewhere else?
                m_connections.Enqueue(conn);
                Monitor.Pulse(key);
            }
        }
    }
}
